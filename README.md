# Auto install description
Shell Script to auto cover some repetitive process on instantiating a local budibase container.

# How to run it
  1 - Clone this repo in any directory on your server: 
    
    git clone https://gitlab.com/budibase/auto-install.git

  2 - Open it :

    cd auto-install

  3 - Give it execution permissions:

    chmod +x budibase-install.sh

  4 - Run it:

    sh budibase-install.sh

# Do it manually
### 1 - Create budibase user:

```bash
adduser budibase
```

### 2 - Create docker group:

```bash
groupadd docker
```

### 3 - Add budibase to group docker:

```bash
usermod -a -G docker budibase
```

### 4 - Check if budibase is in docker group:

```bash
grep docker /etc/group
```

### 5 - Update the `apt` package index and install packages to allow `apt` to use a HTTPS repository:

```bash
sudo apt-get install \
ca-certificates \
curl \
gnupg \
lsb-release
```

### 6 - Add Docker’s official GPG key:

```bash
 curl -fsSL [https://download.docker.com/linux/ubuntu/gpg](https://download.docker.com/linux/ubuntu/gpg) | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

### 7 - Set up the **stable** repository.

```bash
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### 8 - Update ubuntu:

```bash
 sudo apt-get update
```

### 9 - Install Docker dependencies and docker-compose:

```bash
sudo apt-get install docker-ce docker-ce-cli [containerd.io](http://containerd.io) docker-compose
```

### 10 - Login as budibase:

```bash
su budibase
```

### 11 - Install NVM (Node Version Manager):

```bash
curl -o- [https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh](https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh) | bash
```

### 12 - Load the recent updated shell variables:

```bash
source ~/.bashrc
```

### 13 - Install NodeJS LTS:

```bash
nvm install --lts
```

### 14 - Install Budibase’s CLI with NPM:

```bash
npm i -g @budibase/cli
```

### 15 - Setup basic budi hosting with default configuration:

```bash
budi hosting --init
```

### 16 - Deploy budibase instance:

```bash
budi hosting --start
```