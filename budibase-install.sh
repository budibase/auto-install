update_sys() {
  apt-get update && apt-get upgrade -y
}

enable_install_over_HTTPS() {
  apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
}

add_docker_gpg() {
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
}

setup_docker_stable() {
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
}

install_docker() {
  apt-get install docker-ce docker-ce-cli containerd.io docker-compose
}

create_budibase_user() {
  useradd -g users -G wheel,developers budibase
}

login_budibase() {
  su budibase
}

install_nvm() {
  cd ~ && curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash && source ~/.bashrc
}

nvm_install_node() {
  nvm install --lts
}

npm_install_budibase_cli() {
  npm i -g @budibase/cli
}

create_init_alias() {
  echo 'alias budi-start="budi hosting --start"' >>.bashrc && source ~/.bashrc
}

budi_hosting_init() {
  budi hosting --init
}

main() {
  update_sys
  enable_install_over_HTTPS
  add_docker_gpg
  setup_docker_stable
  update_sys
  install_docker
  create_budibase_user
  login_budibase
  nvm_install_node
  npm_install_budibase_cli
  create_init_alias
  budi_hosting_init && clear && echo "DIGITE budi-start para iniciar o serviço do Budibase."
}

main